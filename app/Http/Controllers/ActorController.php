<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\RequestLogger;
use Illuminate\Support\Facades\Storage;

class ActorController extends Controller
{
    public function profile()
    {
        header('Content-Type: application/ld+json');
        $profile = [
            "@context" => $this->getContext(),
            "id" => "https://" . request()->getHost() . "/u/" . env('LOCAL_ACTOR_USERNAME', 'jack') . "",
            "type" => "Person",
            "following" => "https://" . request()->getHost() . "/u/" . env('LOCAL_ACTOR_USERNAME', 'jack') . "/following",
            "followers" => "https://" . request()->getHost() . "/u/" . env('LOCAL_ACTOR_USERNAME', 'jack') . "/followers",
            "inbox" => "https://" . request()->getHost() . "/u/" . env('LOCAL_ACTOR_USERNAME', 'jack') . "/inbox",
            "outbox" => "https://" . request()->getHost() . "/u/" . env('LOCAL_ACTOR_USERNAME', 'jack') . "/outbox",
            "featured" => "https://" . request()->getHost() . "/u/" . env('LOCAL_ACTOR_USERNAME', 'jack') . "/collections/featured",
            "featuredTags" => "https://" . request()->getHost() . "/u/" . env('LOCAL_ACTOR_USERNAME', 'jack') . "/collections/tags",
            "preferredUsername" => env('LOCAL_ACTOR_USERNAME', 'jack'),
            "name" => env('LOCAL_ACTOR_USERNAME', 'jack'),
            "summary" => "a test account",
            "url" => "https://" . request()->getHost() . "/u/" . env('LOCAL_ACTOR_USERNAME', 'jack') . "",
            "manuallyApprovesFollowers" => true,
            "publicKey" => [
                "id" => "https://" . request()->getHost() . "/u/" . env('LOCAL_ACTOR_USERNAME', 'jack') . "#main-key",
                "owner" => "https://" . request()->getHost() . "/u/" . env('LOCAL_ACTOR_USERNAME', 'jack') . "",
                "publicKeyPem" =>  Storage::get('public.pem')
            ],
            "tag" => [],
            "attachment" => [],
            "endpoints" => [
                "sharedInbox" => "https://" . request()->getHost() . "/inbox"
            ]
        ];
        RequestLogger::log($profile);

        return response($profile)
            ->header('Content-Type', "application/ld+json");
    }

    public function getContext()
    {
        return [
            "https://www.w3.org/ns/activitystreams",
            "https://w3id.org/security/v1",
            [
                "manuallyApprovesFollowers" => "as:manuallyApprovesFollowers",
                "PropertyValue" => "schema:PropertyValue",
                "schema" => "http://schema.org#",
                "value" => "schema:value"
            ]
        ];
    }
}
