<?php

namespace App\Http\Livewire;

use App\Models\Request;
use Livewire\Component;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ApFlow extends Component
{
    public $requests;

    public function render()
    {
        $this->requests = Request::all()->append(['prettyJsonBody', 'prettyJsonBodyResp']);
        $this->requests->map(function (Request $request) {

            $fields = ['respHeaders', 'headers'];

            foreach ($fields as $field) {
                if ($request->{$field} != null) {
                    $request->{$field} = Arr::where($request->{$field}, function ($value, $key) {
                        return is_array($value) ? trim($value[0]) != "" : trim($value) != "";
                    });
                }
            }

            return $request;
        });
        $this->dispatchBrowserEvent('highlight');
        return view('livewire.ap-flow');
    }

    public function trashRequests()
    {
        DB::table('requests')->truncate();
        $this->requests = [];
    }
}
