<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Str;
use App\Helpers\HttpSignature;
use App\Helpers\RequestLogger;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ApSender extends Component
{
    public $activityId;
    public $objectId;
    public $actorUsername;
    public $actorWebfinger = "";
    public $signed = false;
    public $recipient;
    public $recipientProfile;
    public $properties;
    public $responseBody;
    public $responseCode = '?';
    public $discoverError = "";
    public $sendError = "";
    public $sentRequest = null;

    protected $listeners = ['addALineTo'];

    public function mount()
    {
        $this->recipient = env('DEFAULT_SENDER_RECIPIENT', '');
        $this->actorUsername = env('LOCAL_ACTOR_USERNAME', 'jack');

        $props = [
            //id, actor, and to will be automatically generated for finalProps.
            [
                "key" => "type",
                "value" => "Create",
                "readonly" => false
            ], [
                "key" => "to",
                "value" => "https://mas.to/users/rick",
                "readonly" => false
            ], [
                "key" => "object",
                "value" => [
                    //id, attributedTo, and to will be automatically generated for finalProps.
                    [
                        "key" => "type",
                        "value" => "Note",
                        "readonly" => false
                    ], [
                        "key" => "content",
                        "value" => "Hello Rick !!",
                        "readonly" => false
                    ],
                    [
                        "key" => "",
                        "value" => null,
                        "readonly" => false
                    ]
                ],
                "readonly" => false
            ]
        ];
        $this->properties = collect($props);

        $this->actorWebfinger = "@" . $this->actorUsername . "@" . request()->getHost();
    }

    public function getFinalPropertiesProperty()
    {
        //Build the dynamic properties
        $this->activityId = Str::random(10);
        $this->objectId = Str::random(10);

        $props = [];
        $props['@context'] = "https://www.w3.org/ns/activitystreams";
        $props['id'] = "https://" . request()->getHost() . "/activities/" . $this->activityId;
        $props = array_merge($props, $this->mixKeyValues($this->properties));
        $props['actor'] = $this->getActorURI();
        $props['published'] = now();

        $newProps = $props;

        //Dynamic properties if the object exists and is an array
        if (isset($props['object']) && is_array($props['object'])) {
            $object = $props['object'];
            unset($props['object']);
            $props['object']['id'] = "https://" . request()->getHost() . "/objects/" . $this->objectId;
            $props['object'] = array_merge($props['object'], $object);
            $props['object']['attributedTo'] = $this->getActorURI();
            $props['object']['published'] = $props['published'];
            $props['object']['to'] = $props['to'];


            //Remove empty props at sublevel (in the 'object' array)
            //TODO: refactor to have all levels and not only objects
            unset($newProps['object']);
            foreach ($props['object'] as $key => $prop) {
                if (trim($key) != "") {
                    $newProps['object'][trim($key)] = $prop == "" ? null : $prop;
                }
            }
        }

        //Remove empty props at first level
        foreach ($newProps as $key => $prop) {
            if (trim($key) != "") {
                $finalProps[trim($key)] = $prop == "" ? null : $prop;
            }
        }

        return $finalProps;
    }

    public function getPrettyFinalPropertiesProperty()
    {
        $props = $this->finalProperties;
        return json_encode($props, (JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
    }

    public function mixKeyValues($properties)
    {
        $mixedCollection = [];
        foreach ($properties as $prop) {
            if (is_array($prop['value'])) {
                $prop['value'] = $this->mixKeyValues($prop['value']);
            }
            $mixedCollection[$prop['key']] = $prop['value'];
        }
        return $mixedCollection;
    }

    public function render()
    {
        $this->dispatchBrowserEvent('highlight');
        return view('livewire.ap-sender');
    }

    public function addALineTo($field)
    {
        //Todo: add line addition for sub JSON objects
        //Todo: algo: remove last dot char and look for the path with the dot notation.
        $this->properties->push([
            "key" => "",
            "value" => "",
            "readonly" => false
        ]);
    }

    public function resolve()
    {
        //Get Webfinger data
        $recipientId = null;
        $this->discoverError = "";
        $webfinger = $this->recipient;
        $domainPieces = explode("@", $webfinger);
        $domain = $domainPieces[count($domainPieces) - 1];
        if (Str::startsWith($webfinger, "@")) {
            $webfinger = Str::substr($webfinger, 1);
        }
        $webfingerUrl = "https://" . $domain . "/.well-known/webfinger?resource=acct:" . $webfinger;

        try {
            $responseWebfinger = Http::accept("application/ld+json")->get($webfingerUrl);
            RequestLogger::saveRequestWith($responseWebfinger, $webfingerUrl);
            $webfingerData = collect(json_decode($responseWebfinger->body()));
            foreach ($webfingerData['links'] as $link) {
                if (in_array($link->type, ["application/activity+json", "application/ld+json"])) {
                    $recipientId = $link->href;
                    break;
                }
            }

            //Get recipient profile
            try {
                if ($recipientId != null) {
                    $responseProfile = Http::accept("application/ld+json")->get($recipientId);
                    RequestLogger::saveRequestWith($responseProfile, $recipientId);
                    if ($responseProfile->successful() == false) {
                        $this->discoverError = $responseProfile->clientError();
                    }
                    $this->recipientProfile = json_decode($responseProfile->body(), true);
                }
            } catch (ModelNotFoundException $e) {
                $this->discoverError .=  " " . $e->getMessage();
            }
        } catch (ModelNotFoundException $e) {
            $this->discoverError = " " . $e->getMessage();
        }
    }

    public function getActorURI()
    {
        return "https://" . request()->getHost() . "/u/" . $this->actorUsername;
    }

    public function send()
    {
        $this->sentRequest = null;
        if ($this->recipientProfile != null) {
            $this->sendError = "";

            //Build required info
            $key = Storage::get('private.pem');
            $keyId = $this->getActorURI() . "#main-key";
            $url = $this->getRecipientInbox();
            $data = $this->finalProperties;

            if ($this->signed) {
                $headers = HttpSignature::sign($key, $keyId, $url, $data);
            } else {
                $headers = [];
            }

            $response = Http::withHeaders($headers)->post($url, $data);
            $this->sentRequest = RequestLogger::saveRequestWith($response, $url, 'POST', $data, $headers);
            $this->responseBody = $response->body();
            $this->responseCode = $response->status();
        } else {
            $this->sendError = "No actor loaded. Discover one first.";
        }
    }

    public function getRecipientInbox()
    {
        return $this->recipientProfile['inbox'] ?? null;
    }
}
