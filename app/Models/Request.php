<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Request extends Model
{
    use HasFactory;

    protected $casts = [
        'respHeaders' => 'array',
        'respBody' => 'array',
        'body' => 'array',
        'headers' => 'array',
    ];

    protected $fillable = [
        'type',
        'method',
        'url',
        'userAgent',
        'ip',
        'status',
        'headers',
        'respHeaders',
        'body',
        'respBody'
    ];

    public function getPrettyJsonBodyAttribute()
    {
        return json_encode($this->body, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    public function getPrettyJsonBodyRespAttribute()
    {
        $respBody = $this->respBody;
        if ($respBody != null) {
            if (is_array($respBody) == false) {
                $respBody = json_decode($respBody, true);
                if ($respBody == null) {
                    return $this->respBody;   //if not an array a not a JSON parsable string, this is just a string, so just return it.
                }
            }
            return json_encode($respBody, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        }
        return $respBody;
    }

    public function getSourceAttribute()
    {
        //Try to extract the source form the User-Agent. Ex: " http.rb/5.0.4 (Mastodon/3.5.1; +https://indieweb.social/)"
        $userAgent = $this->userAgent;
        $url = Str::substr($this->userAgent, strpos($userAgent, '+'));   //extract URL
        //$domain = Str::substr($source, strpos($source, "https://") + 8, strlen($source) - 8 - 2);
        $domain = Str::before(Str::after($url, '//'), '/');
        if ($domain == "" || preg_match("/[a-z]{3,}\.[a-z]{2,}/", $domain) !== 1) {
            if ($this->ip == null) {
                return $userAgent;
            } else {
                return $this->ip;
            }
        }
        return $domain;
    }

    public function getDestinationAttribute()
    {
        return Str::before(Str::after($this->url, '//'), '/');
    }
}
