<?php

namespace App\Helpers;

use App\Models\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Client\Response;

class RequestLogger
{
    //Log incoming requests  if pass filters
    public static function log($content = null)
    {
        Log::info("passing in log()");
        $onlyAPRequests = false;
        if (str_contains(request()->url(), "/livewire") == false && request()->header('User-Agent') != "Symfony") {
            if ($onlyAPRequests) {
                if (Str::startsWith(request()->header('Accept'), ["application/ld+json", "application/activity+json"])) {
                    self::saveRequest(null, $content);
                }
            } else {
                self::saveRequest(null, $content);
            }
        }
    }

    //Save the outgoing request in db if given, or build the incoming request object and save it in db 
    protected static function saveRequest($request = null, $content = null)
    {
        Log::info("passing in saveRequest()");
        if ($request == null) {
            $request = [
                'type' => 'in',
                'method' => request()->method(),
                'url' => request()->fullUrl(),
                'userAgent' => request()->header('User-Agent'),
                'ip' => request()->ip(),
                'status' => null,
                'headers' => self::removeUselessHeaders(getallheaders()),
                'respHeaders' =>  self::parseHeadersToArray(headers_list()),
                'body' => empty(request()->all()) ? null : request()->all(),
                'respBody' => $content ?? ''
            ];
        }
        return Request::create($request);
    }

    //Build the request object of the outoing request
    public static function saveRequestWith(Response $response, $url, $method = "GET", $body = null, $headers = null)
    {
        $request = [
            'type' => 'out',
            'method' => $method,
            'url' => $url,
            'userAgent' => 'apdebug',    //todo replace with env value
            'ip' => null,
            'status' => $response->status(),
            'headers' => $headers,
            'respHeaders' => $response->getHeaders(),
            'body' => $body,
            'respBody' => $response->body()
        ];
        return self::saveRequest($request);
    }

    public static function parseHeadersToArray($indexedArray)
    {
        $assocArray = [];
        foreach ($indexedArray as $item) {
            $assocArray[Str::before($item, ':')] = Str::after($item, ':');
        }
        return $assocArray;
    }

    public static function removeUselessHeaders($headers)
    {
        $keys = ['X-Forwarded-For', 'X-Forwarded-Proto', 'X-PageKite-Port'];    //keys to remove

        $headers = Arr::except($headers, $keys);

        return $headers;
    }
}
