<?php

use App\Models\Request;
use Illuminate\Support\Str;
use App\Helpers\RequestLogger;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Config;

use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\ActorController;

Route::get('/', function () {
    return view('welcome');
});


//Route::get('/.well-known/webfinger?resource={webfinger}', function () {
//	Log::debug(request());
//});

if (str_contains(request()->url(), "/livewire") == false && request()->header('User-Agent') != "Symfony") {
    Log::info(request()->url());
    Log::info(request()->method());
    Log::info(request()->header('Accept'));
    Log::info(request()->header('Content-Type'));
    Log::info(request()->header('User-Agent'));
    Log::info(request()->all());
}

Route::get('/u/' . env('LOCAL_ACTOR_USERNAME', 'jack'), [ActorController::class, 'profile']);

Route::get('/.well-known/webfinger', function () {
    $array = [
        "subject" => "acct:" . env('LOCAL_ACTOR_USERNAME', 'jack') . "@" . request()->getHost() . "",
        "aliases" => [
            "https://" . request()->getHost() . "/web/u/" . env('LOCAL_ACTOR_USERNAME', 'jack') . "",
            "https://" . request()->getHost() . "/u/" . env('LOCAL_ACTOR_USERNAME', 'jack') . ""
        ],
        "links" => [
            [
                "rel" => "http://webfinger.net/rel/profile-page",
                "type" => "text/html",
                "href" => "https://" . request()->getHost() . "/web/u/" . env('LOCAL_ACTOR_USERNAME', 'jack') . ""
            ],
            [
                "rel" => "self",
                "type" => "application/activity+json",
                "href" => "https://" . request()->getHost() . "/u/" . env('LOCAL_ACTOR_USERNAME', 'jack') . ""
            ],
            [
                "rel" => "http://ostatus.org/schema/1.0/subscribe",
                "template" => "https://" . request()->getHost() . "/authorize_interaction?uri={uri}"
            ]
        ]
    ];
    RequestLogger::log($array);

    return response($array, 200);
});
Route::get('/.well-known/host-meta', function () {
    echo 'as';
    RequestLogger::log("webfinger host meta");
    return response('', 200);
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/', function () {
        return view('sender');
    })->name('sender');
    Route::get('/flow', function () {
        return view('flow');
    })->name('flow');
});

Route::post('u/' . env('LOCAL_ACTOR_USERNAME', 'jack') . '/inbox', function () {
    RequestLogger::log(env('LOCAL_ACTOR_USERNAME', 'jack') . " inbox");
    return response(env('LOCAL_ACTOR_USERNAME', 'jack') . ' inbox', 200);
});

Route::any('{any}', function ($any) {
    RequestLogger::log("route /{$any} not found sorry...");
    return "route not found sorry...";
});
