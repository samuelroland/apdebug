<div class="flex items-end">
    <h1 style="font-family: 'Fira Code';" class="text-3xl font-serif p-1 rounded-lg">
        <span class="text-green-700 font-semibold">AP</span><span class="font-semibold ml-[2px] text-sky-700">debug</span>
    </h1>
    <span class="text-xs italic text-cblue"
        title="The version {{ config('app.version') }} was released on the {{ config('app.version_date')->format('d.m.Y H:i') }}.">{{ config('app.version') }}</span>
</div>
