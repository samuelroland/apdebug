<div>

    <div class="flex flex-wrap space-x-2 py-3">
        <div class="shadow-md bg-blue-100 p-2">
            <h2 class="text-xl">Actors</h2>
            <div class=" ">
                <div class="flex-1">
                    <div class="mb-2">
                        <div>Sender</div>
                        <input class="w-12" type="text"
                            wire:model="actorUsername"><span>{{ '@' . request()->getHost() }}</span>
                    </div>
                </div>

                <div>
                    <div class="mb-2">
                        <div class="mr-1">Recipient</div>
                        <input class="w-72" type="text" wire:model="recipient">
                        <br>
                        <button wire:click="resolve()" class="mt-2 btn-action">Discover</button>
                    </div>

                    <div class="mb-2">
                        <h4 class="mr-1 text-lg">Recipient information</h4>
                        @if ($recipientProfile != null)
                            <div class="max-w-xs">
                                @if ($recipientProfile['icon'] != null)
                                    <div>
                                        <img src="{{ $recipientProfile['icon']['url'] }}" alt="recipient icon"
                                            class="h-16 w-16">
                                    </div>
                                @endif
                                <div>
                                    <strong>Name</strong>: {{ $recipientProfile['name'] ?? '?' }}
                                </div>
                                <div>
                                    <strong>Username</strong>: {{ $recipientProfile['preferredUsername'] ?? 'none' }}
                                </div>
                                <div>
                                    <strong>Biography</strong>
                                    <div class="text-sm biography">
                                        {{-- TODO: refactor to avoid XSS --}}
                                        {{-- {{ strip_tags(str_replace('<br>', '\n', $recipientProfile['summary'] ?? '')) }} --}}
                                        {!! $recipientProfile['summary'] !!}
                                    </div>
                                </div>
                                <div>
                                    <strong>Actor ID</strong> <br>
                                    <span class="text-url">
                                        {{ $recipientProfile['id'] ?? '?' }}
                                    </span>
                                </div>
                                <div>
                                    <strong>Actor Inbox</strong> <br>
                                    <span class="text-url">
                                        {{ $recipientProfile['inbox'] ?? '?' }}
                                    </span>
                                </div>
                            </div>
                        @else
                            <span class="text-info">No recipient loaded...</span>
                        @endif
                        <span class="italic text-red-600">{{ $discoverError }}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="shadow-md bg-blue-100 p-2">
            <h2 class="text-xl mb-1">Body edition</h2>
            <x-json-table :properties="$properties" livewirePrefix="."></x-json-table>
            <div>
                <h3 class="text-lg mb-1 mt-3">Dynamic fields</h3>
                <div class="">
                    @foreach (collect($this->getFinalPropertiesProperty())->only(['id', 'actor', 'published']) as $key => $value)
                        <div><strong>{{ $key }}</strong>: <code class="text-sm">{{ $value }}</code>
                        </div>
                    @endforeach

                    @if ($this->getFinalPropertiesProperty()['object'] != null)
                        <strong>object</strong>:
                        <div class="ml-2">
                            @foreach (collect($this->getFinalPropertiesProperty()['object'])->only(['id', 'attributedTo', 'published']) as $key => $value)
                                <div><strong>{{ $key }}</strong>: <code
                                        class="text-sm">{{ $value }}</code>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="shadow-md bg-blue-100 p-2">
            <div class="flex">
                <h2 class="text-xl mb-1 flex-1">Final request</h2>
                <div class="flex items-center"><button class="btn-action" wire:click="send">Send!</button></div>
            </div>
            <div class="">
                <div>
                    <span class="font-semibold">POST</span>
                    <span
                        class="text-url break-all">{{ $this->getRecipientInbox() ?? 'Recipient inbox URL loaded...' }}</span>
                </div>
                <div class="mb-2">
                    <input class="" type="checkbox" wire:model="signed">
                    <span class="ml-1">Sign the request with {{ $actorUsername }}'s private key</span>
                </div>
                <x-code class="max-w-3xl" type="json" :small="false">{{ $this->prettyFinalProperties }}
                </x-code>
            </div>

            @if ($sentRequest != null)
                <div class="mt-3">
                    <h2 class="text-xl">Response</h2>
                    <div class="flex">
                        <div class="flex-1">
                            <div>
                                <strong>From</strong>:
                                <span>{{ $sentRequest->destination }}</span>
                            </div>
                            <div>
                                <strong>Status</strong>:
                                <span
                                    class="text-lg font-bold {{ $sentRequest->status < 300 && $sentRequest->status >= 200 ? 'text-green-800' : 'text-orange-600' }}">
                                    {{ $sentRequest->status }}</span>
                            </div>
                            <div>
                                <strong>Response body</strong>:
                                @if ($sentRequest->respBody == '')
                                    <span class="text-info">Empty response body</span>
                                @endif
                                <code class="ml-1 text-sm whitespace-pre">{{ $sentRequest->respBody }}</code>
                            </div>
                            <div>

                            </div>
                            <div class="text-error">{{ $sendError }}</div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
