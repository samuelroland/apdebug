<div>
    <div class="my-3 bg-blue-100 shadow-md" wire:poll.2s="$refresh()">
        <div class="flex" x-data="{ req: null, displayHeaders: false }">
            <div class="p-2 lg:pr-10 sm:pr-5 pr-2">
                <div class="flex">
                    <div class="flex-1 flex items-center">
                        <h2 class="text-xl">Requests inspection</h2>
                        <span class="ml-5">{{ $requests->count() }} requests</span>
                    </div>
                    <div>
                        <button class="btn py-0" wire:click="trashRequests" @click="req = null">Trash</button>
                    </div>
                </div>

                <hr class="my-1 border-t border-cblue">

                <div class="py-1 flex-1 max-h-[73vh] overflow-scroll">
                    @forelse ($requests as $request)
                        <div :class="req == null || req != {{ $request->id }} ? '' : '!bg-orange-300'"
                            class="bg-orange-200 hover:bg-orange-300 rounded p-2 h-min mb-2 relative shadow-lg border border-orange-400 {{ $request->type == 'out' ? 'mr-0' : 'ml-0' }} cursor-pointer max-w-lg"
                            @click="req = {{ $request->id }}">
                            <div class="flex items-center">
                                <span class="font-semibold mr-2  md:text-lg">{{ strtoupper($request->type) }}</span>
                                <div class="flex-1">

                                    <span class="mr-2 italic whitespace-nowrap flex items-center">
                                        {{ request()->getHost() }}
                                        <div class="mx-2">
                                            @if ($request->type == 'in')
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                                    viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                        d="M7 16l-4-4m0 0l4-4m-4 4h18" />
                                                </svg>
                                            @else
                                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none"
                                                    viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                                    <path stroke-linecap="round" stroke-linejoin="round"
                                                        d="M17 8l4 4m0 0l-4 4m4-4H3" />
                                                </svg>
                                            @endif
                                        </div>
                                        {{ $request->type == 'in' ? $request->source : $request->destination }}
                                    </span>
                                </div>
                                <div class="text-sm w-max flex items-center">
                                    <div
                                        class="flex items-center justify-center font-semibold h-7 w-7 border border-orange-400">
                                        {{ $request->id }}
                                    </div>
                                </div>
                            </div>


                            <div class="text-xs md:text-sm lg:text-base">
                                <span class=" font-semibold mr-1">{{ $request->method }}</span>
                                <span class="text-url break-all">{{ $request->url }}</span>
                            </div>
                            <div class="flex">
                                <div class="flex flex-1">
                                    @if ($request->status != null)
                                        <div class="text-sm md:text-base mr-2 label">
                                            Status:
                                            {{ $request->status }}</div>
                                    @endif

                                    @if ($request->method == 'POST' && isset($request->body['type']))
                                        <div class="text-sm md:text-base mr-2 label">
                                            Activity type: {{ $request->body['type'] }}
                                        </div>
                                    @endif

                                </div>
                                <div class="w-max mr-2 text-xs my-1">{{ $request->created_at->diffForHumans() }}
                                </div>

                            </div>
                        </div>
                    @empty
                        <span class="text-info">No request for the moment</span>
                    @endforelse
                </div>
            </div>

            <!-- Details panel at the right -->
            <div x-show="req != null" x-cloak class="w-full max-w-full overflow-hidden">
                <div class="max-w-full flex-1 w-full bg-blue-200 md:p-2 py-2 px-1 h-full" x-show="req != null">
                    <div class="flex mb-2">
                        <h2 class="flex-1 text-xl">Details inspection</h2>
                        <div class="flex items-center justify-center font-semibold h-7 w-7 bg-orange-500">
                            @foreach ($requests as $request)
                                <div :hidden="req != {{ $request->id }}">
                                    {{ $request->id }}
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="mb-2">
                        @foreach ($requests as $request)
                            <div :hidden="req != {{ $request->id }}">
                                <span class="font-semibold">{{ $request->method }}</span>
                                <span class="text-url break-all">{{ $request->url }}</span>
                            </div>
                        @endforeach
                    </div>
                    <hr class=" mb-1 border-t border-cblue">

                    <div class="w-full max-h-[73vh] xl:flex overflow-hidden">

                        {{-- Body zones --}}
                        <div class="xl:w-[60%] overflow-scroll">
                            {{-- Request body displayed only when the method is not GET --}}
                            @foreach ($requests as $request)
                                @if ($request->method != 'GET')
                                    <div class="min-w-full mb-2" :hidden="req != {{ $request->id }}">

                                        <h3 class="flex-1 text-lg">Request body</h3>

                                        <div class="text-sm w-full overflow-hidden">
                                            @if ($request->body != null)
                                                <x-code type="json" x-cloak>
                                                    {{ $request->prettyJsonBody }}</x-code>
                                            @else
                                                <div class="italic text-gray-100 p-2 bg-dark rounded mr-2">no body sent
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                            {{-- Response body --}}
                            <div class="min-w-full mb-2 max-w-full ">
                                <div class="flex">
                                    <h3 class="flex-1 text-lg">Response body</h3>
                                </div>
                                @foreach ($requests as $request)
                                    <div class="text-sm w-full overflow-hidden overflow-x-scroll"
                                        :hidden="req != {{ $request->id }}">
                                        @if ($request->respBody != null)
                                            <x-code type="json" x-cloak>{{ $request->prettyJsonBodyResp }}
                                            </x-code>
                                        @else
                                            <div class="italic text-gray-100 p-2 bg-dark rounded mr-2">no body received
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        {{-- Headers zones --}}
                        <div class="xl:w-[40%] overflow-scroll">
                            <div class="text-lg">Headers</div>
                            <div>

                                {{-- Request headers --}}
                                <div>
                                    <div class="min-w-full mb-2">
                                        <div class="flex">
                                            <h3 class="flex-1 text-base">Requests headers</h3>
                                        </div>
                                        <div
                                            class="text-sm bg-gray-100 p-2 rounded overflow-hidden break-all max-w-full">
                                            @foreach ($requests as $request)
                                                <div :hidden="req != {{ $request->id }}">
                                                    @if ($request->headers != null)
                                                        @foreach ($request->headers as $index => $header)
                                                            <div>
                                                                @if (Arr::isAssoc($request->headers))
                                                                    <span
                                                                        class="font-extrabold">{{ $index }}</span>:
                                                                @endif
                                                                <span class="text-gray-700">{{ $header }}</span>
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        <div class="text-info">no logged headers</div>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>

                                    </div>
                                </div>

                                {{-- Response headers --}}
                                <div class="min-w-full mb-2">
                                    <div class="flex">
                                        <h3 class="flex-1 text-base">Response headers</h3>
                                    </div>
                                    <div class="text-sm w-full overflow-hidden bg-gray-100 rounded p-2">
                                        @foreach ($requests as $request)
                                            <div :hidden="req != {{ $request->id }}">
                                                @if ($request->respHeaders != null)
                                                    @foreach ($request->respHeaders as $index => $header)
                                                        <div>
                                                            @if (Arr::isAssoc($request->respHeaders))
                                                                <span
                                                                    class="font-extrabold">{{ $index }}</span>:
                                                            @endif
                                                            <span
                                                                class="text-gray-700">{{ is_array($header) ? $header[0] : $header }}</span>
                                                        </div>
                                                    @endforeach
                                                @else
                                                    <div class="text-info">no logged response headers</div>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
