<x-app-layout>
    <h1 class="text-2xl mb-2">Sender</h1>
    <p>Build a custom ActivityPub request to an actor's inbox, with HTTP signature and dynamic fields handled for you.
    </p>
    @livewire('ap-sender')
</x-app-layout>
