@props(['type', 'bodyField', 'prettyBodyField', 'emptyMessage' => 'empty', 'small' => true])
<div {{ $attributes }} class="max-w-full mr-2 max-h-[500px] overflow-scroll">
    <pre><code class="w-full max-w-full overflow-hidden rounded {{ $small ? 'text-xs md:text-sm' : 'text-xs sm:text-sm md:text-base' }} language-{{ $type }}">{{ $slot }}</code></pre>
</div>
