@props(['properties' => null, 'livewirePrefix' => null])
<div>
    <table class="w-full max-w-4xl json-table">
        <thead>
            <tr>
                <th>Key</th>
                <th>Value</th>
                {{-- <th>ReadOnly</th> --}}
            </tr>
        </thead>
        <tbody>
            @forelse ($properties as $key => $prop)
                <tr wire:key="table-prop-{{ $key }}">
                    <td class="w-32"><input class="w-32 {{ $prop['readonly'] ? 'bg-gray-300' : '' }}" type="text"
                            {{ $prop['readonly'] ? 'readonly disbaled' : '' }}
                            wire:model.lazy="properties{{ $livewirePrefix }}{{ $key }}.key"></td>
                    <td class="w-max">
                        @if (is_array($prop['value']))
                            <x-json-table :properties="$prop['value']" :livewirePrefix="$livewirePrefix . $key . '.value.'">
                            </x-json-table>
                        @else
                            <input class="w-full {{ $prop['readonly'] ? 'bg-gray-300' : '' }}" type="text"
                                {{ $prop['readonly'] ? 'readonly disbaled' : '' }}
                                wire:model.lazy="properties{{ $livewirePrefix }}{{ $key }}.value">
                        @endif
                    </td>
                    {{-- <td class="w-10"><input type="checkbox" 
                            wire:model="properties{{ $livewirePrefix }}{{ $key }}.readonly"></td> --}}
                </tr>
            @empty
                No properties found
            @endforelse
            <tr>
                <td colspan="3" class="hover:font-bold cursor-pointer"
                    wire:click="$emitUp('addALineTo' , '{{ $livewirePrefix }}')">
                    Add a line</td>
            </tr>
        </tbody>
    </table>
</div>
