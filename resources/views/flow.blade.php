<x-app-layout>
    <h1 class="text-2xl mb-2">Flow</h1>
    <p>Inspect the flow of incoming and outgoing ActivityPub requests on this server.</p>
    @livewire('ap-flow')
</x-app-layout>
