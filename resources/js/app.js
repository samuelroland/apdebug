require('./bootstrap');

import Alpine from 'alpinejs';

import hljs from 'highlight.js/lib/core';
import json from 'highlight.js/lib/languages/json';
hljs.registerLanguage('json', json);

import 'highlight.js/styles/night-owl.css';

window.Alpine = Alpine;
window.hljs = hljs;

Alpine.start();
