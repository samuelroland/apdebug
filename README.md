# APdebug
### A set of tools to debug ActivityPub requests. You can inspect requests, see the flow in real time, run arbitrary requests with HTTP signature handled for you, ...
![logo](docs/imgs/logo.png)

This is still in development and this is not ready for real usage... it needs some adjustements, fixes and enhancements before being able to be useful. It needs some refactoring and some automated tests before I can publish the version 1.0.0. This tool started for a week-long challenge I made to discover ActivityPub and produce a video about it. You can see more about the project and provide feedbacks [in the post on SocialHub](https://socialhub.activitypub.rocks/t/student-dev-making-the-first-steps-with-activitypub-and-doing-a-video/).

*Some documentation on how to install and how to use will be provided when ready in [docs.md](docs/docs.md).*

## Features
### Sender
This section let you send a custom request and build a JSON body easily through dynamic fields, and a table with key-values.

![ApSender panel with the possibility to send a custom request](docs/imgs/apsender.png)

### Flow
The flow page is made to see the flow of incoming and outgoing requests on this server. The local server can be made publicly accessible with a service like Ngrok or Pagekite to get a domain.

![ApFlow panel with the possibility to see all the details about each requests from or to our server](docs/imgs/apflow.png)

## Other ideas of features
- Be able to connect to a remote server and see the flow of requests, could be very useful to inspect requests of other real ActivityPub applications. This would need to build an adapter for the remote app (for a Pixelfed servers for ex.) to have an authenticated API route to be able to read latest or all requests each minute.
- Send requests with custom request headers, if needed
- Possibility to replay a request (and to change it a bit just before sending it). Inspired by Firefox Devtools feature "Edit and Resend". This would be awesome.
- Test and build HTTP signature, with visual schemas to understand the algo, with different signature specs, to better understand how it work and which app is compatible with which.
- Have automated tests and fake remote servers
- A Fetch panel to fetch links easily with and without authentication (authentication would be useful to see the content of the inbox)
- Clickable links in JSON: would be great to fetch received URL to see what's behind.
- Create and manage a list of actors, to vary the authors of requests. Manage their profile information and/or directly see/modify the profile URL result.
- A way to filter requests among different fields and dynamic fields and requests content.

## Credits and licence
APdebug is a free software licenced under [AGPLv3](LICENSE) or later.

### Credits
- Code
  - HTTP signature code: todo Pixelfed
- Icons
    - From heroicons.com - MIT licenced

TODO: complete this list